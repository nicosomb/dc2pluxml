<?php
/*
 * Import d'article de dotclear 2.1-5 vers pluxml
 * Version : 0.1
 */
include('prepend.php');

echo '<p>Avant tout import penser à faire une sauvegarde complète de votre installation dotclear, pluxml. Penser également à paramétrer votre pluxml correctement<br /> <strong>Tout les modes d\'import écrase les données déjà existante dans pluxml</strong></p>';

echo '<form action="index.php?import_type=xml" method="post"><fieldset>';
echo '<legend>Import par xml</legend>';
echo '<p><strong>Cette méthode importe uniquement les articles sans tenir compte des catégories. L\'import est réalisé dans le contenue des articles</strong></p>';
echo '<label>Merci d\'indiquer l\'adresse du flux de vos articles : </label>';
plxUtils::printInput('xml_adresse', $xml_adresse, 'text', '50-255');
echo '<br /><input type="submit" name="update" value="Enregistrer"/>';
echo '</fieldset></form>';

// echo '<form action="index.php?import_type=mysql" method="post"><fieldset>';
// echo '<legend>Import par mysql</legend>';
// echo '<p><strong>Cette méthode importe les articles en tenant compte des catégories, chapô, contenue, commentaires.<br />ATTENTION vous devez commencé par importer les catégories</strong></p>';
// echo '<label>Adresse du serveur mysql : </label>';
// plxUtils::printInput('mysql_host', $mysql_host, 'text', '50-255');
// echo '<br /><label>Nom de la base de données : </label>';
// plxUtils::printInput('mysql_db', $mysql_db, 'text', '50-255');
// echo '<br /><label>Nom d\'utilisateur mysql : </label>';
// plxUtils::printInput('mysql_user', $mysql_user, 'text', '50-255');
// echo '<br /><label>Mot de passe mysql : </label>';
// plxUtils::printInput('mysql_pass', $mysql_pass, 'text', '50-255');
// echo '<br /><label>Préfix de table utilisé par dotclear, généralement c\'est dc_  : </label>';
// plxUtils::printInput('mysql_prefix', $mysql_prefix, 'text', '50-255');
// echo '<br /><input type="submit" name="update" value="Enregistrer"/>';
// echo '</fieldset></form>';

$import_type = $_GET['import_type'];
if($import_type == 'xml') {
    // Si vous avez choisi la méthode xml vous devez
    // indiquer l'adresse du flux de votre blog
    $xml_adresse = file_get_contents($_POST['xml_adresse']);

    $xml = new SimpleXMLElement($xml_adresse);
    $i = 0;

    $liste = array();
    foreach($xml->entry as $item) {
    	$liste[] = $item;
    }
	$liste = array_reverse($liste); 

    foreach($liste as $item) {
    	$i++;
        $data = array();
    
        $data['title'] = $item->title;
        $data['chapo'] = $url = '';
        $data['content'] = $item->content;
        $data['author'] = '000';
    
        $date = explode('-', $item->updated);
        $data['year'] = $date[0];
        $data['month'] = $date[1];
        $data['day'] = substr($date[2], 0, 2);
        $time = explode('T', $item->updated);
        $time = explode(':', $time[1]);
        $hour = $time[0];
        $minute = substr($time[1], 0, 2);
        $data['time'] = $hour.':'.$minute;
    
        $data['catId'] = array('home');
        $data['url'] = $item->title;
        $data['artId'] = str_pad($i, 4, "0", STR_PAD_LEFT);
        $data['allow_com'] = $plxAdmin->aConf['allow_com'];
        $data['title_page'] = $item->title;
    
        $plxAdmin->editArticle($data, $data['artId']);    
    }
    $msg = 'Import des articles terminés';
    header('Location: '.$plxAdmin->aConf['racine'].'core/admin/?&msg='.urlencode($msg));
}
?>