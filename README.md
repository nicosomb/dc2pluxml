# dc2pluxml

wp2pluxml vous permet de convertir le contenu de votre blog Wordpress en un blog PluXml.
**Attention**, ce script est à exécuter en local et non en environnement de production (selon la taille de votre blog Dotclear, la génération peut prendre du temps).

J'ai repris le code de flipflip http://www.blogoflip.fr/?article63/plugin-dotclear2pluxml

## Changelog

* corrections de quelques morceaux de code pour que ça fonctionne avec la dernière version de PluXml
* suppression de la vérification de l'authentification. Ce qui veut dire que : N'IMPORTE QUI PEUT UTILISER CE SCRIPT, donc ne laissez pas trainer ça sur votre site. 
* désactivation de la partie import par mysql

## Installation et utilisation
* Mettez le dossier dotclear2pluxml dans /plugins
* Allez sur http://votresitepluxml/plugins/dotclear2pluxml
* Renseignez le flux RSS de votre site Dotclear. Validez. 

Vous avez récupéré une partie des billets de votre blog Dotclear. 
Pour en récupérer plus, connectez vous sur votre site Dotclear et dans les paramètres du blog, changez la valeur de "Afficher X billets par flux de syndication".

## TODO
* récupération des commentaires
* récupération des catégories
* ...